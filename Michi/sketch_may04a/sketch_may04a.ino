


int messwert=0; //Unter der Variablen "messwert" wird später der Messwert der Lichtschranke gespeichert.



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Die Kommunikation mit dem seriellen Port wird gestartet. Das benötigt man, um sich den ausgelesenen Wert im serial monitor anzeigen zu lassen.

}

void loop() {
  // put your main code here, to run repeatedly:
  messwert=digitalRead(10);
  Serial.print("Messwert:      ");
  Serial.println(messwert);
  
}
